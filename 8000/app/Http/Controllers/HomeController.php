<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        // Logika tambahan Anda
        $data = "Ini adalah data tambahan.";
        return view('home', ['data' => $data]);
    }
}