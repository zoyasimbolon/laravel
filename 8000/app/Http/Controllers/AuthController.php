<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
// AuthController.php
public function showRegistrationForm()
{
    return view('register');
}

public function processRegistration(Request $request)
{
    // Proses data pendaftaran dan alihkan ke halaman selamat datang
    return redirect()->route('welcome', [
        'firstName' => $request->input('firstName'),
        'lastName' => $request->input('lastName'),
    ]);
}

public function welcome(Request $request)
{
    // Validasi dan proses data pendaftaran
    $firstName = $request->input('firstName');
    $lastName = $request->input('lastName');
    
    // Redirect ke halaman welcome dengan mengirim data nama
    return view('welcome', compact('firstName', 'lastName'));
}

}
