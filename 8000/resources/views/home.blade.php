<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day 1 Zoya</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
            text-align: center; /* Center-align all content */
        }

        h1 {
            margin-top: 30px;
        }

        h3 {
            text-align: center;
        }

        p {
            text-align: center;
        }

        ul, ol {
            text-align: left;
            display: inline-block;
        }

        li {
            margin-top: 10px;
        }

        a {
            display: block;
            text-align: center;
            text-decoration: none;
            background-color: #0074b7;
            color: #fff;
            padding: 10px 20px;
            border-radius: 3px;
            margin: 20px auto;
            width: 200px;
        }

        a:hover {
            background-color: #00568b;
        }
    </style>
</head>
<body>
    <h1>Garuda Cyber Institute</h1>
    <h3>Jadilah Programer Handal Bersama GC-INS</h3>
    <p>Grow Together With Garuda Cyber Institute</p>
    <h3>Syarat dan Ketentuan</h3>
    <ul>
        <li>Tamatan SMA/SMK</li>
        <li>Tamatan Perguruan Tinggi</li>
        <li>Pekerja IT</li>
        <li>Freelancer</li>
    </ul>
    <h3>Cara Bergabung</h3>
    <ol>
        <li>Kunjungi website GC-INS</li>
        <li>Register</li>
        <li>Lakukan pembayaran</li>
    </ol>
    <a href="{{ route('register') }}">Daftar Sekarang</a>
</body>
</html>
